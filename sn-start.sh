#!/bin/bash
#Make sure only root can run our script
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi
#Run the following as root on your server (without the comment #):
#wget -O - https://gitlab.com/sacrednetwork/public/raw/master/sn-start.sh --no-check-certificate | bash
echo "This script will install git so you can install your repos."
#Install Server utilities
apt-get update
apt-get install -y ca-certificates apt-transport-https git

mkdir -p /share/sacrednet
cd /share/sacrednet
git clone https://gitlab.com/sacrednetwork/scripts.git
#git clone git@gitlab.com:sacrednetwork/scripts.git
tee -a <<EOF >> /root/.bashrc
#SNS
export SNS='/share/sacrednet/scripts'
PATH=$PATH:/share/sacrednet/scripts
export PATH
EOF
source /root/.bashrc

echo ':: Complete'
exit 0
