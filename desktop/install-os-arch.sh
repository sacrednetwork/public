#!/bin/bash

#Make sure only root can run our script
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

#Download install script:
#pacman -Sy wget git nano
#wget https://gitlab.com/sacrednetwork/public/raw/master/desktop/install-os-arch.sh
#echo "Update keys and refresh pacman on live install media"
#pacman-key --init
#pacman-key --populate archlinux
#pacman-key --refresh-keys
#pacman -Sy

echo 'Script : Install Arch Linux : Start : 0.2'
echo "This script assumes EFI installation, and Internet Connection already working on an arch live install media, as well as language and keyboard configuration is default."
echo "To create logfile please Run Script with the following ending:  2>&1 | tee install-os-arch.log"

echo "Continue? yes | no" && read continue
if [ ${continue} = "no" ]; then
    echo "Exiting Script."
    exit
else
    echo "Continuing Script."
fi


echo "Setting Network Time Protocol to True"
timedatectl set-ntp true

echo "The formatting process assumes specific partition numbering, update this script if trying to accomidate other configurations, otherwise installation is on 1 drive dedicated to arch install with 2 partitions, 1 EFI and 2 EXT4 as Root"

fdisk -l
echo "Enter Target Install Storage (example: /dev/nvme0n1 or /dev/sda)" && read storagedrive
echo "fdisk commands: [d delete, n new, t type, w write, m help]"
echo "Make the Following Partitions (minimal partion requirement):
/dev/sda1   type:EFI (1)
/dev/sda2   type:Linux (20)"
fdisk $storagedrive
echo "Enter full storage path including p (example: /dev/nvme0n1p or /dev/sda)" && read storagedrive
echo "Formating Partitions"
mkfs.ext4 ${storagedrive}2
mkfs.fat -F32 ${storagedrive}1

echo "Mounting Partitions."
mount ${storagedrive}2   /mnt/
mkdir /mnt/boot/
mount ${storagedrive}1   /mnt/boot/

echo "Pacstrap Installing Packages: base linux linux-firmware man-db man-pages texinfo vim"
echo "Enter Additional Packages Now (space between multiple):" && read pacins
pacstrap -K /mnt base linux linux-firmware man-db man-pages texinfo vim ${pacins}
echo "Generating Fstab"
genfstab -U /mnt >> /mnt/etc/fstab

echo "Performing Chroot on /mnt"
echo "Install Bootloader"
arch-chroot /mnt bootctl install
arch-chroot /mnt cp /usr/share/systemd/bootctl/loader.conf /boot/loader/loader.conf
arch-chroot /mnt cp /usr/share/systemd/bootctl/arch.conf /boot/loader/entries/arch.conf

echo "Update keys and refresh pacman on new install chroot"
arch-chroot /mnt pacman-key --init
arch-chroot /mnt pacman-key --populate
arch-chroot /mnt pacman-key --refresh
arch-chroot /mnt pacman -Sy

echo "Enter ucode vendor: intel-ucode | amd-ucode" && read ucodevendor
arch-chroot /mnt pacman -S $ucodevendor

echo "Update script to use UUID if possible in the boot loader arch.conf"
tee -a << EOF > /mnt/boot/loader/entries/arch.conf
title Arch Linux
linux /vmlinuz-linux
initrd /${ucodevendor}.img
initrd /initramfs-linux.img
options root=${storagedrive}2 rw
EOF

echo "Creating Swap File"
echo "Enter Swap File Size (1GB=1024 | 2GB=2048 | 4GB=4096 | 6GB=6144 | 8GB=8192)" && read swapsize
arch-chroot /mnt dd if=/dev/zero of=/swapfile bs=1M count=${swapsize} status=progress
arch-chroot /mnt chmod 600 /swapfile
arch-chroot /mnt mkswap /swapfile
arch-chroot /mnt swapon /swapfile
echo "/swapfile none swap defaults 0 0" >> /mnt/etc/fstab
echo "vm.swappiness=10" >> /mnt/etc/sysctl.d/99-swappiness.conf
arch-chroot /mnt sysctl -w vm.swappiness=10
echo "vm.vfs_cache_pressure=50" >> /mnt/etc/sysctl.d/99-swappiness.conf
arch-chroot /mnt sysctl vm.vfs_cache_pressure=50

echo "Enter Timezone, example: America/Chicago" && read timezone
arch-chroot /mnt timedatectl set-timezone ${timezone}
arch-chroot /mnt hwclock --systohc

echo "script assumes local to be en_US.UTF-8"
echo "en_US.UTF-8 UTF-8" >> /mnt/etc/locale.gen
echo "LANG=en_US.UTF-8" >> /mnt/etc/locale.conf
arch-chroot /mnt localectl set-locale LANG=en_US.UTF-8
arch-chroot /mnt locale-gen

echo "Enter Hostname" && read hostname
arch-chroot /mnt echo "${hostname}" > /mnt/etc/hostname
#use tee to add to file
tee -a << EOF > /mnt/etc/hosts
127.0.0.1 localhost
::1 localhost
127.0.1.1 ${hostname}.localdomain ${hostname}
EOF
# #Enable to override the DNS defaults
# #cp /etc/resolv.conf /mnt/etc/resolv.conf
# echo "Enter DNS IP" && read dnsip
# echo "${dnsip}" >> /mnt/etc/resolv.conf
# #There is another option to append to STUB file

echo "Installing Desktop Enviornment"
echo "Enter Desktop: kde | gnome" && read desktop
if [ ${desktop} = 'kde' ]; then
    echo "Installing Xorg"
    arch-chroot /mnt pacman -S xorg-server
    echo "Choose Video Driver:"
    arch-chroot /mnt pacman -Ss xf86-video
    echo "Enter Video Drivers to install example: xf86-video-intel vulkan-intel vulkan-mesa-layers" && read displaydrivers
    arch-chroot /mnt pacman -S $displaydrivers
    echo "Configure Xorg"
    cp /mnt/usr/share/X11/xorg.conf.d/* /mnt/etc/X11/xorg.conf.d/.
    echo "Installing KDE"
    arch-chroot /mnt pacman -S plasma-meta
    arch-chroot /mnt pacman -S kde-applications-meta kwalletmanager
    arch-chroot /mnt pacman -S packagekit-qt5 flatpak fwupd
    arch-chroot /mnt pacman -S sddm sddm-kcm
    mkdir /mnt/etc/sddm.conf.d/
    cp /usr/lib/sddm/sddm.conf.d/default.conf /mnt/etc/sddm.conf.d/default.conf
    arch-chroot /mnt systemctl enable sddm
    arch-chroot /mnt systemctl enable NetworkManager
elif [ ${desktop} = 'gnome' ]; then
    echo "Installing Gnome"
    arch-chroot /mnt pacman -S gnome
    arch-chroot /mnt pacman -S gnome-extra
    arch-chroot /mnt pacman -S gdm
    arch-chroot /mnt systemctl enable gdm
    arch-chroot /mnt systemctl enable NetworkManager
else
    echo "you did not enter correct option, we will skip desktop installation."
    echo "We will setup a wired network device with DHCP"
    ip link
    echo "Enter Wired Network Device (enp1s0)" && read netdev
    echo "
    [Match]
    Name=${netdev}
    [Network]
    DHCP=yes
    " > /mnt/etc/systemd/network/20-wired.network
    echo "Enabling Networkd and Resolved to start at boot."
    arch-chroot /mnt systemctl enable systemd-networkd.service
    arch-chroot /mnt systemctl enable systemd-resolved.service
fi

arch-chroot /mnt pacman -S bluez
arch-chroot /mnt systemctl enable bluetooth.service

#Install Sudo
echo "Installing Sudo"
arch-chroot /mnt pacman -S sudo
arch-chroot /mnt chown -c root:root /etc/sudoers
arch-chroot /mnt chmod -c 0440 /etc/sudoers

#User Management:
echo "Enter Username:" && read username
arch-chroot /mnt useradd -m $username
echo "Add user to wheel group"
arch-chroot /mnt usermod -aG wheel $username
echo "Enter User Password"
arch-chroot /mnt passwd $username
echo "Add wheel group to sudoers file"
#Always edit it with visudo to prevent errors.
#EDITOR=nano visudo
echo "%wheel   ALL=(ALL) ALL" >> /mnt/etc/sudoers
echo "Disable Root Password"
arch-chroot /mnt passwd -l root

echo "Enter Additional Packages to install, example: htop ncdu pydf" && read additionalpackages
arch-chroot /mnt pacman -S ${additionalpackages}

echo "Installing AUR manager"
echo "Enter AUR Pacakges to Install example: systemd-boot-pacman-hook arch-wiki-man wikiman gdm3setup" && read aurpackages
echo "Enter AUR manager: paru | yay" && read auragent
if [ ${auragent} = 'paru' ]; then
    echo "Installing paru"
    arch-chroot /mnt pacman -S --needed base-devel git
    arch-chroot /mnt git clone https://aur.archlinux.org/paru.git /opt/paru/
    arch-chroot /mnt chown -R ${username}:users /opt/paru
    arch-chroot /mnt su ${username} -P -c 'cd /opt/paru/ && makepkg -si'
    arch-chroot /mnt paru -S $aurpackages
elif [ ${auragent} = 'yay' ]; then
    echo "Installing yay"
    arch-chroot /mnt pacman -S --needed base-devel git
    arch-chroot /mnt git clone https://aur.archlinux.org/yay.git /opt/yay/
    arch-chroot /mnt chown -R ${username}:users /opt/yay
    arch-chroot /mnt su ${username} -P -c 'cd /opt/yay/ && makepkg -si'
    arch-chroot /mnt yay -S $aurpackages
else
    echo "you did not enter correct option, we will skip AUR manager installation."
fi

echo "Please Unmount & Reboot"
#maybe ask to delete log file if it was created
#umount -R /mnt
#reboot -t 30

echo 'Script : Install Arch Linux : Complete'
exit 0
